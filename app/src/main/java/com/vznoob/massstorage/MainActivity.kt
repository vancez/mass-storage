package com.vznoob.massstorage

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader

class MainActivity : AppCompatActivity() {
    private val massStorageFile = "/config/usb_gadget/g1/functions/mass_storage.0/lun.0/file"
    private val defaultMassStorage = "/system/etc/usb_drivers.iso"

    companion object {
        private const val ACTIVITY_CHOOSE_FILE = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestPermission()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        updateMassStorageTextView()

        val btnChoose: Button = findViewById(R.id.button_main_choose)
        btnChoose.setOnClickListener {
            val chooseFile = Intent(Intent.ACTION_GET_CONTENT)
            chooseFile.addCategory(Intent.CATEGORY_OPENABLE)
            chooseFile.type = "application/x-iso9660-image"
            val intent = Intent.createChooser(chooseFile, "Choose a file")
            startActivityForResult(intent, ACTIVITY_CHOOSE_FILE)
        }
        val btnEject: Button = findViewById(R.id.button_main_eject)
        btnEject.setOnClickListener {
            ejectMassStorage()
            updateMassStorageTextView()
        }
        val tvMassStorage: TextView = findViewById(R.id.textview_main_massstorage)
        tvMassStorage.setOnClickListener {
            updateMassStorageTextView()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_OK) return
        if (requestCode == ACTIVITY_CHOOSE_FILE) {
            val uri = data?.data ?: return
            val docUri = uri.path?.split(":") ?: return
            val file = File(Environment.getExternalStorageDirectory(), docUri[1])
            if (file.exists()) {
                setMassStorage(file.path)
            }
            updateMassStorageTextView()
        }
    }

    private fun requestPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                0
            )
        }
    }

    private fun updateMassStorageTextView() {
        val tv: TextView = findViewById(R.id.textview_main_massstorage)
        val massStorage = getMassStorage()
        tv.text = if (massStorage != "") massStorage else "empty"
    }

    private fun runAsRoot(command: String): String {
        val cmd = arrayOf("su", "-c", command)
        val proc = Runtime.getRuntime().exec(cmd)

        val stdOut = BufferedReader(InputStreamReader(proc.inputStream))
        val stdErr = BufferedReader(InputStreamReader(proc.errorStream))

        val output = StringBuffer()
        var read: Int
        val buffer = CharArray(4096)
        while (stdOut.read(buffer).let { read = it; it > 0 }) {
            output.append(buffer, 0, read)
        }
        while (stdErr.read(buffer).let { read = it; it > 0 }) {
            output.append(buffer, 0, read)
        }
        stdOut.close()
        stdErr.close()
        proc.waitFor()

        return output.toString().trim()
    }

    private fun setMassStorage(isoFile: String): Boolean {
        val output = runAsRoot("echo $isoFile >$massStorageFile")
        Log.d("vznoob", output)
        return output == ""
    }

    private fun getMassStorage(): String {
        return File(massStorageFile).readText().trim()
    }

    private fun ejectMassStorage(): Boolean {
        return setMassStorage("")
    }
}
